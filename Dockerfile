FROM tamlongdo/nginx-php72:base

RUN mkdir /app
WORKDIR /app

# Composer
COPY composer.json /app/

COPY .env.example /app/.env

COPY . /app/

RUN chown -R www-data:www-data /app

# Install composer
RUN cd /app/ && composer install --no-dev --prefer-dist --optimize-autoloader && composer clear-cache

# Run artisan
RUN cd /app/ && php artisan key:generate && php artisan config:cache && php artisan cache:clear

# run test codecept
RUN cd /app/ && composer dump-autoload && ./vendor/bin/codecept run unit